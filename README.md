#FranceImmo

FranceImmo est un site internet qui gère les transactions immobilières de ses clients, de la mise en vente de biens à la gestion locative, en passant par l’estimation ou la recherche de biens. Ses collaborateurs sont généralement des agents immobiliers 

Principales fonctionnalités :

- Espace admin 
- Menu : 
- Gestion des biens (liste des biens + formulaire de création et modification) 
- Candidatures, pouvoir accepter / refuser avec un message explicatif 
- Espace client 
- Inscription gratuite + simple vérification de l’adresse email 
- Menu : 
- Mes documents : ajoute ses documents (fiche de paie, justificatif de domicile, etc…) 
- Mes favoris : pouvoir liker une annonce pour la retrouver rapidement 
- Mes demandes : lorsqu’il postule pour un appartement, il peut suivre ici l’avancement de ses dossiers 
- Recherche de biens - Trier par : date, prix, popularité 
- Filtrer par : loyer min/max, ville, surface en m2 
- Sur la fiche d’un bien : 
- Carousel de photos 
- Carte Google Maps 
- Pouvoir partager l’annonce sur Facebook
